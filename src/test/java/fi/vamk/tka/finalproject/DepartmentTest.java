package fi.vamk.tka.finalproject;

import static org.assertj.core.api.Assertions.*;
import static org.junit.Assert.assertEquals;

import org.apache.commons.collections4.IterableUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class DepartmentTest {
    @Autowired
    private DepartmentRepository repository;

    @Test
    public void postGetDeleteAtteandance() {
        Iterable<Department> begin = repository.findAll();
        // System.out.println(IterableUtils.size(begin));
        // given
        Department att = new Department("ABCD");
        System.out.println("ATT: " + att.toString());
        // test save
        Department saved = repository.save(att);
        System.out.println("SAVED: " + saved.toString());
        // when
        Department found = repository.findByName(att.getName());
        System.out.println("FOUND " + found.toString());
        // then
        assertThat(found.getName()).isEqualTo(att.getName());
        repository.delete(found);
        Iterable<Department> end = repository.findAll();
        // System.out.println(IterableUtils.size(end));
        assertEquals((long) IterableUtils.size(begin), (long) IterableUtils.size(end));
    }

}