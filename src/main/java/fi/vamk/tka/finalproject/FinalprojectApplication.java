package fi.vamk.tka.finalproject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class FinalprojectApplication {
	@Autowired
	private DepartmentRepository repository;

	public static void main(String[] args) {
		SpringApplication.run(FinalprojectApplication.class, args);
	}

	@Bean
	public void initData() {
		Department d = new Department("IT");
		repository.save(d);
		Iterable<Department> dps = repository.findAll();
		for (Department de : dps) {
			System.out.println(de.toString());
		}
	}

}
