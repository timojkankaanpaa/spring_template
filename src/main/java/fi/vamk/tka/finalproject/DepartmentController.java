package fi.vamk.tka.finalproject;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "Department CRUD.", description = "Provides RESTful API for department.")
@RestController
public class DepartmentController {
    @Autowired
    private DepartmentRepository repository;

    @ApiOperation(value = "Return all departments")
    @GetMapping("/departments")
    public Iterable<Department> list() {
        return repository.findAll();
    }

    @ApiOperation(value = "Return one department by id")
    @GetMapping("/departments/{id}")
    public Optional<Department> get(@PathVariable("id") int id) {
        return repository.findById(id);
    }

    @ApiOperation(value = "Save a department to database")
    @PostMapping("/department")
    public Department create(@RequestBody Department item) {
        return repository.save(item);
    }

    @ApiOperation(value = "Update the department to database")
    @PutMapping("/department")
    public Department update(@RequestBody Department item) {
        return repository.save(item);
    }

    @ApiOperation(value = "Delete the department from database")
    @DeleteMapping("/department")
    public void delete(@RequestBody Department item) {
        repository.delete(item);
    }
}