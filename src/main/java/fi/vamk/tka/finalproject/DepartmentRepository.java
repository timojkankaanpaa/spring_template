package fi.vamk.tka.finalproject;

import org.springframework.data.repository.CrudRepository;

public interface DepartmentRepository extends CrudRepository<Department, Integer> {
    public Department findByName(String name);
}