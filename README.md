<h2>Spring Boot backend</h2>
This is an example how to implement Spring backend. The main features are
<ul>
<li>Adding limited (=hard coded use OAuth, LDAP or Cognito instead) user authentication</li>
<li>Adding Swagger dependencies and Bean to enable documentation</li>
<li>Uses HSQL to uses as simple as possible database (data is not persisted)</li>
<li>Creating entity and repository classes</li>
<li>Creating CRUD in controller with initial documentation</li>
<li>Enabling CRUD in security (see WebSecurityConfig now too open)</li>
<li>Testing the repository</li>
</ul>

<h2>How to use</h2>
To extend the functionality, new entity, repository and controller classes are needed.

<pre>mvn test (to run the test cases)</pre>

<pre>mvn exec:java (to run backend)</pre>

<pre>mvn package (to run the jar: java jar target/finalproject-0.0.1-SNAPSHOT.jar)</pre>
